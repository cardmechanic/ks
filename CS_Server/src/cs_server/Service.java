
package cs_server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server Class for establashing Connections, implemented as Singleton
 * 
 * @author Robert Wagner
 */
public class Service implements Runnable{
    
    ServerSocket Server;
    private static Service instance;
    int port;
    boolean locked;
    
    private Service(int port){
        this.port = port;
        locked = false;
    }
    /**
    * Thread run-method. Initializing Socket and waiting for incoming requests
    */
    @Override
    public void run(){
        InputService check = new InputService();
        Thread k = new Thread(check);
        k.start();
        
        try{
        Server = new ServerSocket(port);
        System.out.println("Service started at "+ Server.getInetAddress()+":"+Server.getLocalPort());
        while(locked == false){
            
            Socket b = Server.accept();
            System.out.println("Connected to"+b.getInetAddress());
            Thread p = new Thread(new Handler(Server,b));
            p.start();
        }
        
        }catch(IOException e){
            
            System.out.println(e.getMessage());
        }
    }
    
    public void stopServer(){
        try{
            locked = true;
           
            if(!this.Server.isClosed() )
                this.Server.close();
                   
            System.out.println("Shutdown finished");
                
        }catch (IOException e){
            System.out.println(e.getMessage());
            
        }
    }
    /**
     * geting Single Instance of Service Class
     * @return Instance of Service
     */
    public static synchronized Service getInstance () {
        
    if (Service.instance == null) {
        Service.instance = new Service (50000);
    }
    return Service.instance;
  }
}
