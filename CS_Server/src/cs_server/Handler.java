
package cs_server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for handling incoming requests
 * @author Robert Wagner
 */
public class Handler implements Runnable {

    /**
     * Socket for specific client
     */
    Socket client;
    /**
     * Serversocket with connection information
     */
    ServerSocket socket;
    
    public Handler(ServerSocket socket, Socket current){
        this.socket = socket;
        this.client = current;
    }
    @Override
    
    /**
     * thread run method for handling the request in a new thread, sending the response and waiting for another one.
     */
    public void run() {
        
        System.out.println("Starting requesthandle");
        PrintWriter out = null;
        boolean quit = false;
        while (!quit){
            try{
                BufferedReader bufferedReader = 
                new BufferedReader(
                  new InputStreamReader(
                    client.getInputStream()));

                out = new PrintWriter( client.getOutputStream(), true );
                char[] buffer = new char[100];
                
                int count = bufferedReader.read(buffer, 0, 100); 
                if(count != -1){
                    String message = new String(buffer, 0, count);
                    String[] vals = message.split("\\s");
                    System.out.println("Message recieved: "+ message );
                    if(vals.length > 0 && vals[0].equals("exit")){
                        if ( !client.isClosed() ) {
                            System.out.println("--- Ende Handler:ServerSocket close");
                            try {
                              client.close();
                              return;
                            } catch ( IOException e ) {
                                return;
                            }
                        }
                    }
                    String result = "";
                    for (String val : vals) {
                        try{
                            result += getData(val)+System.lineSeparator();
                        }catch(IOException e){
                            result = "Error while getting Data";
                            break;
                        }
                    }
                    result = "Weather in "+ message + "\n\t"+result;
                    result += System.lineSeparator() + "type 'exit' to quit or another request";
                    out.println(result);
                    System.out.println("Communication handled");
                }
                else{
                    out.println("invalid input");
                }
            }catch(IOException e){
                if(!client.isClosed() )
                    try{
                        client.close();
                    }
                    catch(IOException ex){
                    }
                System.out.println("Connection closed");
                return;
            }
        }
    }
    /**
     * alling weather api for recieving neccessary data
     * @param plz nme of the city to get data from
     * @return string with weather data
     * @throws IOException 
     */
    public String getData(String plz) throws IOException{
        
        String urlApi = "http://api.openweathermap.org/data/2.5/weather?q="+plz+"&APPID=b04cac7567d33ea55059addcde5e1823";
       
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlApi);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        
        if(conn.getResponseCode()>205 || conn.getResponseCode() <200)
            return "invalid Request";
        
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
           result.append(line);
        }
        rd.close();
        Pattern p1 = Pattern.compile("\\{(\"temp\".*?)\\}");
        Matcher m = p1.matcher(result.toString());
         if (m.find()) {
     
           result = new StringBuilder((m.group(0))); 
           String out = result.toString();
           out = out.replaceAll("\"", "").replaceAll("\\{", "").replaceAll("\\}", "").replaceAll(",", "\n\t");
           return out;
         }
        return result.toString();
    }
    
}
