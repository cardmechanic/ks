
package cs_server;

import java.util.Scanner;

/**
 * Initializing Serverclass, Thread and checking for stopcommand
 * @author Robert Wagner
 */

public class CS_Server {

  
    public static void main(String[] args) {
      
        Thread t = new Thread(Service.getInstance());
        t.start();
    }
    
}
