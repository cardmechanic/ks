
package cs_server;

import java.util.Scanner;

/**
 * just a little Thread for catching inputs. Currently only for shutdown the Server
 * @author Robert Wagner
 */
public class InputService implements Runnable {

    @Override
    public void run() {
      System.out.println("Type quit to shutdown Server");
      Scanner scanner = new Scanner(System.in);
      String state = scanner.next();
      
      while(!"quit".equals(state) ){
      }
      Service.getInstance().stopServer();
    }
    
}
