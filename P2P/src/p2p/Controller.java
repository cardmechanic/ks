
package p2p;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;


public class Controller implements Initializable {
    
    private Pane rootpane;
    private Service service;
    @FXML private TextArea output;
   
    
    public void setData(){
        ArrayList<String> data = service.getData();
        
        for(int i = 0; i < data.size(); i++){
            
        }
    }
    public void setPane(Pane root){
        
        this.rootpane = root;
    }
    public Pane getRoot(){
        
        return rootpane;
    }
    
    @FXML public void start(){
       this.service = new Service(10003, output);
       Thread t1 = new Thread(this.service);
       t1.start();
    }
    @FXML public void quit(){
      
    }
              
    public void initialize( URL url, ResourceBundle resourcebundle) {
         
        
        
    }

    public static  Controller getInstance() throws Exception{
       	
        FXMLLoader loader = new FXMLLoader();
        
        loader.setLocation(Controller.class.getResource("frame.fxml"));
        Pane rootpane = (Pane)loader.load();
        Controller ct = loader.<Controller>getController();
        ct.setPane(rootpane);
        return ct;
           
    }
}
