
package p2p;

import java.util.ArrayList;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.PortUnreachableException;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Base64;
import javafx.animation.AnimationTimer;
import javafx.animation.Timeline;
import javafx.scene.control.TextArea;


public final class Service implements Runnable{
    
    private DatagramSocket peer;
    private int port;
    private String sendString;
    private ArrayList<String> peers;
    private ArrayList<String> data;
    private AnimationTimer timer;
    private double time;
    private double preftime;
    TextArea out;
            
    public Service(int port, TextArea out){
        this.out = out;
        this.port = port;
        this.data = new ArrayList<>();
        boolean used = false;
        
        while(!used){
            
            try{
                peer = new DatagramSocket(this.port);
                used = true;
                
            }catch(SocketException e){
                ++this.port;
            }
        }
        peers = new ArrayList<>();
        peers.add("127.0.0.1:"+this.port );
        peers.add("127.0.0.1:"+(this.port+1) );
        
        setData();
        sendString = createFromList();
        
        time = 0;
        preftime = 0;
        
        timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                
                knockPeers(l);
            }
 
        };
        
    }
    

    @Override
    public void run()  {
     
       timer.start();
       
        while(true){
            try {
                byte[] dats = new byte[ 2048 ];
                
                DatagramPacket packet = new DatagramPacket( dats, dats.length );
                peer.receive( packet );
                byte[] realData = new byte[packet.getLength()];
                System.arraycopy(packet.getData(), packet.getOffset(), realData, 0, packet.getLength());
                DataRequests handler = new DataRequests();
                
                if(handler.handleRequest(peer, this.peers, this.data, realData))
                    sendString = createFromList();
                
            } catch (IOException ex) {
               
            }
        }
    }
    public ArrayList<String> getList(){
        return peers;
    }
    
    public ArrayList<String> getData(){
        return data;
    }
        public void setData(){
        
        for(int i = 0; i < 3; i++){
            
            int max = Data.data.length;
            int rand = (int)(max*Math.random());
            this.data.add(Data.data[rand]);
            this.out.setText(this.data.get(i));
        }
    }
        
    public void knockPeers(double time){
        
        if(preftime == 0)
            preftime = time;
        
        this.time += (time-preftime);
        
        if(this.time < 500)
            return;

        this.time = 0;

        for(int i = 1; i < peers.size(); i++){
            try{
                InetAddress adr = InetAddress.getByName("localhost");
                String portString = peers.get(i).split(":")[1];
                int portRec = parse(portString);

                if(-1==portRec)
                    continue;

                String s = sendString;
                s = this.checkSum(s);

                byte[] dats = s.getBytes();
                DatagramPacket send = new DatagramPacket( dats, dats.length, adr, portRec );

                peer.send( send );

            }catch(java.net.UnknownHostException e){
                    continue;

            }catch(IOException f){

            }

        }
    }
    public String checkSum(String s){
        try {
             MessageDigest md = MessageDigest.getInstance("MD5");
             byte[] arr = md.digest(s.getBytes());
             String b = Base64.getEncoder().encodeToString(arr);
             return s+b;
            
        }catch (NoSuchAlgorithmException ex) {
            return null;
        }
    }
    
    public String createFromList() {
        
        StringBuilder s = new StringBuilder(peers.get(0));
        
        for(int i = 1; i < peers.size();i++){
            s.append("--");
            s.append(peers.get(i));
        }
        s.append("::");
        s.append(data.get(0));
        out.setText("");      
         for(int i = 1; i < data.size();i++){
            s.append("--");
            s.append(data.get(i));
            out.setText(out.getText()+"\n"+data.get(i));
        }
        return s.toString();
    }
    
    public int parse(String to){
        try{
           return Integer.parseInt(to);
        }catch(NumberFormatException e){
            return -1;
        }
    }
}
