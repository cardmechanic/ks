
package p2p;

import java.lang.reflect.Array;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataRequests {
    
        
    
    public DataRequests() {
        
    }

    public boolean handleRequest(DatagramSocket peer, ArrayList<String> peers, ArrayList<String> data, byte[] datas){
      
       String full =  new String(datas, StandardCharsets.UTF_8);

       String dats = full.substring(0, full.length()-(24));
       String check = full.substring(full.length()-(24), full.length());
       
       if(!checksum(dats, check))
           return false;
       String[] splitByType = dats.split("::");
       String[] peerList = splitByType[0].split("--");
       String[] songList = splitByType[1].split("--");
       return compare(peerList,songList, peers, data);
    }
    
    public boolean compare(String[] peerList,String[] songList, ArrayList<String> peers, ArrayList<String> data){
        boolean written = false;
        
        for (String conn : peerList) {
            
            if(!peers.contains(conn)){
                peers.add(conn);
                written = true;
            }
        }
        for (String song : songList) {
            
            if(!data.contains(song)){
                data.add(song);
                written = true;
            }
        }
        return written;
    }
    
    public boolean checksum(String dats, String check){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
             byte[] arr = md.digest(dats.getBytes());
             String b = Base64.getEncoder().encodeToString(arr);
             return b.equals(check);
        } catch (NoSuchAlgorithmException ex) {
           return false;
        }
    }
}
