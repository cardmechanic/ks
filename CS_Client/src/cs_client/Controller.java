
package cs_client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * Gui Controller for organizing user interface
 * @author Robert Wagner
 */
public class Controller implements Initializable {
    
    private Pane rootpane;
    private Service service;
    @FXML private Label output;
    @FXML private Label expl;
    @FXML private TextField input;
      
    public void setPane(Pane root){
        
        this.rootpane = root;
    }
    public Pane getRoot(){
        
        return rootpane;
    }
    /**
     * startmethod. On Button Click Communication with the Server begins.
     */
    @FXML public void start(){
        if(service == null || !service.state() )
            return;
       
        service.setMessage(input.getText());
        try {
            service.send();
            service.recieve();
        } catch (IOException e) {
             if(!service.closed())
                    try{
                        service.close();}
                    catch(Exception ex){
                    }
            output.setText("error while processing request"); 
            return;
        }
    }
    /**
     * quit method, end the session and the programm
     */
    @FXML public void quit(){
        try{
            service.close();
        }catch(Exception ex){
        }
        System.exit(0);
    }
              
    @Override
    public void initialize( URL url, ResourceBundle resourcebundle) {
         
        service = new Service(50000, output);
    }

    public static  Controller getInstance() throws Exception{
       	
        FXMLLoader loader = new FXMLLoader();
        
        loader.setLocation(Controller.class.getResource("frame.fxml"));
        Pane rootpane = (Pane)loader.load();
        Controller ct = loader.<Controller>getController();
        ct.setPane(rootpane);
        return ct;
           
    }
}
