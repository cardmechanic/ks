
package cs_client;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class CS_Client extends Application{

    
     public void start (Stage stage)throws Exception{
	        
            Controller ctrl = Controller.getInstance();
            if (ctrl == null){
                System.out.println ("File not found");
                return;
            }
            Pane root = ctrl.getRoot();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Weather Client");
            stage.setResizable(false);
            stage.setMinWidth(root.getPrefWidth());
            stage.show();

        }
	 
        public static void main(String[] args) {

            launch(args);
        }

}
