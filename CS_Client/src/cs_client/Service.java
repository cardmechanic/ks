
package cs_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import javafx.scene.control.Label;

/**
 * Establashing Connection to given Server
 * @author Robert Wagner
 */
public class Service {

    private Socket socket;
    private String message;
    boolean state;
    Label output;
    public Service(int port, Label out){
        output = out;
        try {
           this.socket = new Socket("127.0.0.1", port);
           state = true;
        } catch (IOException ex) {
           out.setText("Service not available");
           state = false;
           return;
        }
    }
    /**
     * 
     * @return state of connection 
     */
    public boolean state(){
        return state;
    }
    /**
     * setting Message to send
     * @param message 
     */
    public void setMessage(String message){
        this.message = message;
    }
    /**
     * function for sending request to server
     * @throws IOException 
     */
    public void send() throws IOException{
        if(this.message.equals("")){
            output.setText("no Entry given");
            return;
        }
        PrintWriter printWriter =  new PrintWriter(
                                    new OutputStreamWriter(
                                                socket.getOutputStream()));
        printWriter.print(this.message);
        printWriter.flush();
    }
    
   /**
    * function for recieving data from server
    * @throws IOException 
    */
    public void recieve() throws IOException{
        
        BufferedReader bufferedReader =
             new BufferedReader(
                new InputStreamReader(
                    socket.getInputStream()));
        char[] buffer = new char[100];
        int start = 0;
        int count = bufferedReader.read(buffer, start, 100);
        String inMessage = "";
        
        while(count != -1){
           inMessage +=  new String(buffer, 0, count);
           start +=count;
           if(count <100)
               break;
           count = bufferedReader.read(buffer, 0, 100);
        }
        try{
            output.setWrapText(true);
            output.setText(inMessage);
        }catch(Exception f){
            output.setText("error while processing data");
        }
    }
     
    /**
     * 
     * @return whether the socket is closed 
     */
    public boolean closed(){
        return this.socket.isClosed();
    }
    /**
     * close socket
     * @throws IOException 
     */
     public void close() throws IOException{
        this.socket.close();
    }
}



